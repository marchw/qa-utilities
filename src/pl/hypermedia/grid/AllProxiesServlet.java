package pl.hypermedia.grid;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openqa.grid.internal.ProxySet;
import org.openqa.grid.internal.Registry;
import org.openqa.grid.internal.RemoteProxy;
import org.openqa.grid.web.servlet.RegistryBasedServlet;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

public class AllProxiesServlet extends RegistryBasedServlet {

	private static final long serialVersionUID = 9187869185994252327L;

	public AllProxiesServlet(){
		this(null);
	}
	
	public AllProxiesServlet(Registry registry) {
		super(registry);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}

	protected void process(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.setStatus(200);
		String res = getResponse();
		response.getWriter().print(res);
		response.getWriter().close();

	}

	private String getResponse() throws IOException{
		Gson gson = new Gson();
		ProxySet proxies = this.getRegistry().getAllProxies();
		Iterator<RemoteProxy> iterator = proxies.iterator();
		JsonArray p = new JsonArray();
		while (iterator.hasNext()) {
			RemoteProxy eachProxy = iterator.next();
			List<DesiredCapabilities> cap = eachProxy.getOriginalRegistrationRequest().getCapabilities();
			for(DesiredCapabilities c : cap){
				p.add(gson.toJsonTree(c.asMap()));
			}
		}
		//gson.toJson(eachProxy.getTestSlots());
		return gson.toJson(p);
	}

}
