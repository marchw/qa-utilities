package pl.hypermedia.qa.util;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class ScreenshotMaker {

	private final ExecutorService pool;
	private final List<WebDriver> drivers;

	/**
	 * This method is used only for test purposes. This file should not be runned directly.
	 * @param args Arguments from console.
	 */
	public static void main(String[] args) {
		// System.setProperty("webdriver.chrome.driver",
		// "C:/Users/mmarch01/selenium-2.52.0/chromedriver.exe");
		// WebDriver firefox = new FirefoxDriver();
		// WebDriver chrome = new ChromeDriver();

		URL url = null;
		try {
			url = new URL("http://localhost:4444/wd/hub");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}

		// Chrome on MAC
		DesiredCapabilities dcMacChrome = DesiredCapabilities.chrome();
		dcMacChrome.setPlatform(Platform.MAC);
		WebDriver macChrome = new RemoteWebDriver(url, dcMacChrome);

		// Firefox on MAC
		DesiredCapabilities dcMacFirefox = DesiredCapabilities.firefox();
		dcMacFirefox.setPlatform(Platform.MAC);
		WebDriver macFirefox = new RemoteWebDriver(url, dcMacFirefox);

		// Safari on MAC
		DesiredCapabilities dcMacSafari = DesiredCapabilities.safari();
		dcMacSafari.setPlatform(Platform.MAC);
		WebDriver macSafari = new RemoteWebDriver(url, dcMacSafari);

		// Chrome on Windows
		DesiredCapabilities dcWinChrome = DesiredCapabilities.chrome();
		dcWinChrome.setPlatform(Platform.WINDOWS);
		WebDriver winChrome = new RemoteWebDriver(url, dcWinChrome);

		// Firefox on Windows
		DesiredCapabilities dcWinFirefox = DesiredCapabilities.firefox();
		dcWinFirefox.setPlatform(Platform.WINDOWS);
		WebDriver winFirefox = new RemoteWebDriver(url, dcWinFirefox);

		List<WebDriver> driverList = new ArrayList<WebDriver>();
		driverList.add(macFirefox);
		driverList.add(macChrome);
		driverList.add(macSafari);
		driverList.add(winChrome);
		driverList.add(winFirefox);

		for (WebDriver driver : driverList) {
			driver.get("http://bash.org.pl/latest/");
			// driver.get("http://fitband-dev-cs.hyperlab.pl/fr_FR/#/");
			// Helpers.closeCookieTruste(driver);
		}

		ScreenshotMaker sm = new ScreenshotMaker(driverList);
		sm.setWidth(1600);
		
		File screenshot = new File("screenshots/" + Calendar.getInstance().getTimeInMillis() + ".png");
		if (!screenshot.getParentFile().isDirectory()) {
			screenshot.getParentFile().mkdirs();
		}
		sm.stitchSideBySide(screenshot, sm.takeShots(true));

		for (WebDriver driver : driverList) {
			driver.quit();
		}
	}

	/**
	 * Instantiate object and set {@link ExecutorService} as thread pool to size
	 * of drivers list.
	 * 
	 * @param drivers
	 *            - drivers list on which screenshots will be taken
	 */
	public ScreenshotMaker(List<WebDriver> drivers) {
		this.pool = Executors.newFixedThreadPool(drivers.size());
		this.drivers = drivers;
	}

	/**
	 * Set browser width for every driver.
	 * 
	 * @param width
	 *            - width to be set.
	 */
	public void setWidth(int width) {
		for (WebDriver driver : drivers) {
			int height = driver.manage().window().getSize().getHeight();
			driver.manage().window().setSize(new Dimension(width, height));
		}
	}

	/**
	 * Executes javascript on browser to check viewport.
	 * 
	 * @param driver
	 *            WebDriver to check dimensions
	 * @return List of two longs indicates width and height
	 */
	@SuppressWarnings("unchecked")
	private List<Long> getViewportDimensions(WebDriver driver) {
		File js = new File("resources/getViewport.js");
		List<Long> result = (List<Long>) Helpers.runJSFromFile(driver, js);
		return result;
	}

	/**
	 * Executes javascript on browser to check page height.
	 * 
	 * @param driver
	 *            WebDriver to check dimensions
	 * @return int height of the page
	 */
	private int getPageHeight(WebDriver driver) {
		File js = new File("resources/getPageHeight.js");
		int result = ((Long) Helpers.runJSFromFile(driver, js)).intValue();
		return result;
	}

	/**
	 * Prints provided String at the top of the image. It's quite dummy method -
	 * it doesn't do any calculations, so there is fixed vertical offset of
	 * 100px.
	 * 
	 * @param f
	 *            - file with image
	 * @param meta
	 *            - string to print on the given image
	 * @throws IOException
	 *             if can't read / write to given file
	 */
	private void printMetaData(File f, String meta) throws IOException {
		// TODO adjusting metadata to width
		BufferedImage bi = ImageIO.read(f);
		int heightOffset = 100;

		BufferedImage result = new BufferedImage(bi.getWidth(), bi.getHeight() + heightOffset,
				BufferedImage.TYPE_INT_RGB);
		Graphics g = result.getGraphics();
		g.drawImage(bi, 0, heightOffset, null);
		g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 40));
		g.drawString(meta, 20, 65);
		ImageIO.write(result, "png", f);
	}

	/**
	 * Scroll and stitch page.
	 * 
	 * @param driver
	 * @return BufferedImage with full height screenshot
	 * @throws IOException
	 *             if can't read partial screenshots
	 */
	private BufferedImage scrollAndStich(WebDriver driver) throws IOException {
		List<Long> dimensions = this.getViewportDimensions(driver);
		int vWidth = dimensions.get(0).intValue();
		int vHeight = dimensions.get(1).intValue();
		int pHeight = this.getPageHeight(driver);
		int iterations = pHeight / vHeight;
		int remainder = pHeight % vHeight;

		BufferedImage result = new BufferedImage(vWidth, pHeight, BufferedImage.TYPE_INT_RGB);
		Graphics g = result.getGraphics();

		// draw first screenshot
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		BufferedImage bi = ImageIO.read(screenshot);
		g.drawImage(bi, 0, 0, null);

		// scroll and stitch
		for (int i = 1; i <= iterations; i++) {
			Helpers.scrollBrowser(driver, 0, vHeight);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			bi = ImageIO.read(screenshot);
			g.drawImage(bi, 0, i * vHeight, null);
		}

		// scroll to bottom of the page
		Helpers.scrollBrowser(driver, 0, vHeight);
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}

		// get last screenshot
		screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		bi = ImageIO.read(screenshot);
		g.drawImage(bi, 0, ((iterations - 1) * vHeight) + remainder, null);

		return result;
	}
	
	/**
	 * This method takes screenshots list and write them 
	 * to output file stitched together side by side.
	 * 
	 * @param output
	 * @param screenshots
	 */
	public void stitchSideBySide(File output, List<File> screenshots){
		List<BufferedImage> images = new ArrayList<BufferedImage>();
		try {
			for (File screenshot : screenshots) {
				images.add(ImageIO.read(screenshot));
			}

			int resultWidth = 0;
			int resultHeight = 0;

			for (BufferedImage img : images) {
				resultWidth += img.getWidth();
				resultHeight = Integer.max(resultHeight, img.getHeight());
			}

			BufferedImage result = new BufferedImage(resultWidth, resultHeight, BufferedImage.TYPE_INT_RGB);
			Graphics g = result.getGraphics();
			int currentWidth = 0;

			for (BufferedImage img : images) {
				g.drawImage(img, currentWidth, 0, null);
				currentWidth += img.getWidth();
			}

			ImageIO.write(result, "png", output);

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Cannot stitch screenshots together. Look at tmp folder to find single shots.");
		}
	}

	/**
	 * Take full height screenshot of provided webdriver.
	 * 
	 * @param driver
	 * @return File with single shot
	 */
	private File takeShot(WebDriver driver) {
		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();

		// TODO: Probably it should be reworked to ensure uniqueness of
		// filenames.
		File screenshot = new File("tmp/" + cap.getBrowserName() + "_" + cap.getPlatform() + "_"
				+ Calendar.getInstance().getTimeInMillis() + ".png");

		// ensure that tmp directory exists
		if (!screenshot.getParentFile().isDirectory()) {
			screenshot.getParentFile().mkdirs();
		}

		// Firefox is able to get full height screenshot without scroll and
		// stitch method.
		if (cap.getBrowserName().equals("firefox")) {
			File destination = new File(screenshot.getAbsolutePath());
			File tmp = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.moveFile(tmp, destination);
			} catch (IOException e) {
				System.out.println("Cannot write to location: " + destination.getAbsolutePath());
				e.printStackTrace();
			}
			// For other browsers scroll and stitch
		} else {
			try {
				BufferedImage result = this.scrollAndStich(driver);
				ImageIO.write(result, "png", screenshot);
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Cannot write to location: " + screenshot.getAbsolutePath());
			}
		}

		return screenshot;
	}
	
	/**
	 * Take full height screenshot of provided webdriver and print metadata at top of it.
	 * 
	 * @param driver
	 * @return File with single shot with metadata
	 */
	private File takeShotWithMetaData(WebDriver driver){
		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		
		File screenshot = this.takeShot(driver);
		
		// Print meta data at the top of the image.
		String meta = cap.getBrowserName() + " " + cap.getVersion() + " on " + cap.getPlatform();
		try {
			printMetaData(screenshot, meta);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Cannot print metadata.");
		}
		
		return screenshot;
	}

	/**
	 * This method grabs full height screenshots for driver list provided to
	 * object and return them as list of files.
	 * 
	 *  @return List<File> - list of screenshots
	 */
	public List<File> takeShots() {
		return takeShots(false);
	}
	
	/**
	 * This method grabs full height screenshots for driver list provided to
	 * object and return them as list of files.
	 * 
	 * @param withMetadata - set to true if you want to print metadata at the top af an image
	 * @return List<File> - list of screenshots
	 */
	public List<File> takeShots(boolean withMetadata) {
		List<File> screenshots = new ArrayList<File>(drivers.size());

		// take shots
		for (WebDriver driver : drivers) {
			pool.execute(new Runnable() {
				public void run() {
					if(withMetadata){
						screenshots.add(takeShotWithMetaData(driver));
					} else {
						screenshots.add(takeShot(driver));
					}
				}
			});
		}
		pool.shutdown();
		while (!pool.isTerminated()) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				System.out.println("Operation aborted.");
				pool.shutdownNow();
				Thread.currentThread().interrupt();
			}
		}
		return screenshots;

	}

}
