package pl.hypermedia.qa.util;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Helpers {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "C:/Users/mmarch01/selenium-2.52.0/chromedriver.exe");
		WebDriver driver = new FirefoxDriver();
//		URL url = new URL("http://10.48.50.247:4444/wd/hub");
//		DesiredCapabilities dc = DesiredCapabilities.firefox();
//		dc.setPlatform(Platform.WINDOWS);
//		WebDriver driver = new RemoteWebDriver(url, DesiredCapabilities.safari());
		System.out.println(driver.getClass());
		driver.get("http://www.w3schools.com/js/js_function_definition.asp");
//		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//		FileUtils.copyFile(scrFile, new File("screenshots/test.png"));

		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.manage().window().setSize(new Dimension(400, 400));
		Helpers.scrollBrowser(driver, 0, 1000);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		driver.quit();
	}

	/**
	 * It's utility class, there is no need to instantiate any object.
	 */
	private Helpers() {
	}

	/**
	 * Close truste cookie popup using default selector:
	 * {@code By.xpath("//iframe[contains(@src,'truste.com')]")}
	 * 
	 * @param driver
	 *            - browser window to use
	 * 
	 */
	public static void closeCookieTruste(WebDriver driver) {
		closeCookieTruste(driver, By.xpath("//iframe[contains(@src,'truste.com')]"));
	}

	/**
	 * Close truste cookie popup by given selector.
	 * 
	 * @param driver
	 *            - browser window to use
	 * @param iFrameSelector
	 *            - selector to find cookies iframe
	 * 
	 */
	public static void closeCookieTruste(WebDriver driver, By iFrameSelector) {
		if (isElementPresent(iFrameSelector, driver)) {
			String ctaSelector = "//a[@class='call']";
			driver.switchTo().frame(driver.findElement(iFrameSelector));
			if (isElementPresent(By.xpath(ctaSelector), driver, 2)) {
				driver.findElement(By.xpath(ctaSelector)).click();
			}
			driver.switchTo().defaultContent();
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				// ignore this exception
			}
		}
	}

	/**
	 * Check if element is present with 1 second timeout.
	 * 
	 * @param selector
	 *            - used to search element on page.
	 * @param driver
	 *            - browser to search
	 * @return true if element is present, false otherwise
	 * 
	 * @see Helpers#isElementPresent(By, WebDriver, int)
	 */
	public static boolean isElementPresent(By by, WebDriver driver) {
		return isElementPresent(by, driver, 1);
	}

	/**
	 * Check if element is present.
	 * 
	 * @param selector
	 *            - used to search element on page.
	 * @param driver
	 *            - browser to search
	 * @param timeout
	 *            - time in seconds to wait
	 * @return true if element is present, false otherwise
	 */
	public static boolean isElementPresent(By selector, WebDriver driver, int timeout) {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		} catch (TimeoutException e) {
			return false;
		}
		return true;
	}

	/**
	 * Runs JavaScript code on given WebDriver from given File.
	 * 
	 * @param driver
	 *            - WebDriver to execute JavaScript in
	 * @param file
	 *            - File with JavaScript to execute
	 * @return Object according to JavascriptExecutor docs
	 *         {@link org.openqa.selenium.JavascriptExecutor#executeScript(String, Object...)}
	 * @throws IllegalStateException
	 *             in case if driver doesn't support JavaScript
	 */
	public static Object runJSFromFile(WebDriver driver, File file) throws IllegalStateException {
		if (driver instanceof JavascriptExecutor) {
			JavascriptExecutor js = ((JavascriptExecutor) driver);
			String jsToRun = null;
			try {
				jsToRun = FileUtils.readFileToString(file);
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Can't read file: " + file.getAbsolutePath());
			}
			return js.executeScript(jsToRun);
		} else {
			throw new IllegalStateException("This driver does not support JavaScript!");
		}
	}

	/**
	 * Scroll browser window by provided vertical and horizontal offset
	 * 
	 * @param driver
	 *            - WebDriver to scroll
	 * @param w
	 *            - width in pixels to scroll: positive value to scroll right,
	 *            negative to scroll left
	 * @param h
	 *            - height in pixels to scroll: positive value to scroll down,
	 *            negative to scroll up
	 * @throws IllegalStateException
	 *             in case if driver doesn't support JavaScript
	 */
	public static void scrollBrowser(WebDriver driver, int w, int h) throws IllegalStateException {
		if (driver instanceof JavascriptExecutor) {
			JavascriptExecutor js = ((JavascriptExecutor) driver);
			js.executeScript("window.scrollBy(" + w + ", " + h + ");");
		} else {
			throw new IllegalStateException("This driver does not support JavaScript!");
		}
	}

	/**
	 * Clears local storage and session storage on given driver with javascript.
	 * 
	 * @param driver
	 *            - browser to clear
	 */
	public static void storageClear(WebDriver driver) throws IllegalStateException {
		if (driver instanceof JavascriptExecutor) {
			JavascriptExecutor js = ((JavascriptExecutor) driver);
			js.executeScript("localStorage.clear();");
			js.executeScript("sessionStorage.clear();");
		} else {
			throw new IllegalStateException("This driver does not support JavaScript!");
		}
	}

	/**
	 * Waits for manual reCaptcha resolve and proceed with tests.
	 * 
	 * @param driver
	 *            - browser with captcha
	 * 
	 * @see Helpers#waitForReCaptcha(WebDriver, By)
	 */
	public static void waitForReCaptcha(WebDriver driver) {
		By iFrameSelector = By.xpath("//iframe[contains(@src,'recaptcha')]");
		waitForReCaptcha(driver, iFrameSelector);
	}

	/**
	 * Waits for manual reCaptcha resolve and proceed with tests.
	 * 
	 * @param driver
	 *            - browser with captcha
	 * @param iFrameSelector
	 *            - selector to find captcha iframe
	 */
	public static void waitForReCaptcha(WebDriver driver, By iFrameSelector) {
		if (isElementPresent(iFrameSelector, driver)) {

			driver.switchTo().frame(driver.findElement(iFrameSelector));
			WebElement captcha = driver.findElement(By.xpath("//span[contains(@class, 'recaptcha-checkbox')]"));
			String classes = captcha.getAttribute("class");

			while (!classes.contains("recaptcha-checkbox-checked")) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					driver.switchTo().defaultContent();
					return;
				}
				classes = captcha.getAttribute("class");
			}

			driver.switchTo().defaultContent();
		}
	}
}
